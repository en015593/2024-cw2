#ifndef AREA_H
#define AREA_H

#include <iostream>
#include <fstream>
#include <string>
#include <map>
#include <vector>
#include "Room1.h"

using namespace std ;



class Area
{
    private:
     std::map<std::string, Room*> rooms;

    public:
      void AddRoom(const std::string& name, Room* room);

       Room* GetRoom(const std::string& name);
        void ConnectRooms(const std::string& room1Name, const std::string&
room2Name, const std::string& direction);
 const std::map<std::string, Room*>& GetRooms() const;
 void PrintRoomConnections() const;
        void LoadMapFromFile(const std::string& filename);
};





#endif // AREA_H
