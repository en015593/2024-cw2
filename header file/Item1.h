#ifndef ITEM1_H
#define ITEM1_H

#include <iostream>
#include <string>
#include <map>
#include <vector>

using namespace std ;
class Item {
private:
 std::string name;
 std::string description;
public:
 Item(const std::string& name, const std::string& desc);
  std::string getName() const; // Getter function for name
    std::string getDescription() const;
    void Interact();
};
#endif // ITEM1_H
