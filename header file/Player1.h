#ifndef PLAYER1_H
#define PLAYER1_H


#include <iostream>
#include <string>
#include <map>
#include <vector>
#include <cstdlib> // For rand() and srand()
#include <ctime>
#include "Room1.h"
#include "Character1.h"
using namespace std ;
class Player : public Character {
private:
    Room* location;
public:
    Player(const std::string& name, int health, Room* startingRoom);
    Room* GetLocation();
    void SetLocation(Room* room);
    void PickupItem(const Item& item);
    // Move this declaration inside the Player class
    bool SolveEnigmaticChallenge(const std::vector<std::pair<std::string, std::string>>& questions); // Declare the function
};
  // Declaration of SolveEnigmaticChallenge function
#endif // PLAYER1_H
