#ifndef CHARACTER1_H
#define CHARACTER1_H


#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Room1.h"
using namespace std ;
class Character {
private:
 std::string name;
 int health;
 std::vector<Item> inventory;
public:
 Character(const std::string& name, int health);

  std::vector<Item>& getInventory();
 void TakeDamage(int damage);
 int GetHealth() const;
};

#endif // PLAYER1_H
