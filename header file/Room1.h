#ifndef ROOM1_H
#define ROOM1_H

#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "Item1.h"
#include <algorithm>

using namespace std ;
class Room {
private:
 std::string description;
 std::map<std::string, Room*> exits;
 std::vector<Item> items;
public:
 Room(const std::string& desc);
 std::string GetDescription() const;
 void AddExit(const std::string& direction, Room* room);
 void AddItem(const Item& item);
void RemoveItem(const std::string& itemName);
 const std::vector<Item>& GetItems() const; // Correct declaration
 Item* GetItem(const std::string& name);
 void DisplayItems();
 const std::map<std::string, Room*>& GetExits() const; // Changed function name
    Room* GetExit(const std::string& direction);
};

#endif // ROOM1_H
