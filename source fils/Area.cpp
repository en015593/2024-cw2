#include<map>
#include <sstream>
#include "Area.h"
#include "Room1.h"

//Task 1.2
void Area::AddRoom(const std::string& name, Room* room){
    rooms[name]=room;

}

Room* Area::GetRoom(const std::string& name) {
    if (rooms.find(name) != rooms.end()){
        return rooms[name];
    }
    else {
        return nullptr; // Return nullptr indicating that no room was found
    }
}


void Area::ConnectRooms(const std::string& room1Name, const std::string& room2Name, const std::string& direction) {
    Room* room1 = GetRoom(room1Name);
    Room* room2 = GetRoom(room2Name);
    if (room1 && room2) {
        room1->AddExit(direction, room2);
        std::string oppositeDirection;
        if (direction == "north") {
            oppositeDirection = "south";
        } else if (direction == "south") {
            oppositeDirection = "north";
        } else if (direction == "east") {
            oppositeDirection = "west";
        } else if (direction == "west") {
            oppositeDirection = "east";
        }
        room2->AddExit(oppositeDirection, room1); // Use oppositeDirection here
    }
}

const std::map<std::string, Room*>& Area::GetRooms() const {
    return this->rooms;
}

void Area::PrintRoomConnections() const {
    std::cout << "Room Connections:" << std::endl;
    for (const auto& room : rooms) {
        std::cout << "Room: " << room.first << std::endl;
        std::cout << "Exits: ";
        for (const auto& exit : room.second->GetExits()) {
            std::cout << exit.first << " -> " << exit.second->GetDescription() << ", ";
        }
        std::cout << std::endl;
    }
}


void Area::LoadMapFromFile(const std::string& filename) {
  std::ifstream file;
    file.open(filename);
    if (!file.is_open()) {
        std::cerr << "Error opening file: " << filename << std::endl;
        exit(1);
    }

     std::string line;
    while (std::getline(file, line)) {
        // Print out each line as it's being read


        // Assuming the file format is: room1Name|room2Name|direction
        std::istringstream iss(line);
        std::string room1Name, room2Name, direction;
        std::getline(iss, room1Name, '|');
        std::getline(iss, room2Name, '|');
        std::getline(iss, direction);

        // Print out parsed information


        ConnectRooms(room1Name, room2Name, direction);
    }

    file.close();
}


/*int main(){

    std::cout << "Welcome to the Adventure Game" << std::endl;
// room description
Room startRoom("You are in a dimly lit room.");
Room hallway("You are in a long hallway.");
Room treasureRoom("You have entered a treasure room!");

// Creat items
Item key("Key", "A shiny key that looks important.");
Item sword("Sword", "A sharp sword with a golden hilt.");

// Add items to rooms
 startRoom.AddItem(key);
 treasureRoom.AddItem(sword);
// display the items in rooms
 //std::cout << "Items in the start room:" << std::endl;
   // for (const auto& item : startRoom.getItems()) {
       // std::cout << "- " << item.name << std::endl;
    //}

   // std::cout << "Items in the treasure room:" << std::endl;
   // for (const auto& item : treasureRoom.getItems()) {
        //std::cout << "- " << item.name << std::endl;
   // }
}

*/
