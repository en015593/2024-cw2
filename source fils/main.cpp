#include <iostream>
#include <string>
#include <map>
#include <vector>
#include "../include/Area.h"
#include "../include/Room1.h"
#include "../include/Item1.h"
#include "../include/Character1.h"
#include "../include/Player1.h"

int main() {
    std::cout << "Welcome to the Adventure Game" << std::endl;

    // Create rooms
    // Create rooms
    Room dimlylitRoom("You are in a dimly lit room.");
    Room hallway("You are in a long hallway.");
    Room treasureRoom("You have entered a treasure room!");
    Room joaRoom("This is joaRoom description"); // Add this line

// Add rooms to the area


    // Create items
    Item key("Key", "A shiny key that looks important.");
    Item sword("Sword", "A sharp sword with a golden hilt.");
    Item gloves("gloves","Enhances crafting and tinkering abilities.");
    Item inferno("Inferno","A blade wreathed in flames, scorching foes with each strike.");
    Item vitality("Vitality","An elixir that restores health, mana, and stamina.");
    Item harmony("Harmony","A blade that sings with each swing, disrupting enemy focus.");
    Item candle("candle","light your way");

    // Add items to rooms
    dimlylitRoom.AddItem(key);
    dimlylitRoom.AddItem(gloves);
    treasureRoom.AddItem(vitality);
    treasureRoom.AddItem(candle);
    hallway.AddItem(sword);
    hallway.AddItem(inferno);
   joaRoom.AddItem(harmony);
   joaRoom.AddItem(inferno);

    // Create player
    Player player("Alices", 50, &dimlylitRoom);


    // Create area and load map from file
    Area area;
    area.AddRoom("dimlylitRoom", &dimlylitRoom);
    area.AddRoom("hallway", &hallway);
    area.AddRoom("treasureRoom", &treasureRoom);
    area.AddRoom("joaRoom", &joaRoom); // Add this line
    area.LoadMapFromFile("roomConnection.txt");


    // Debug: Print room connections
  /*   const std::map<std::string, Room*>& rooms = area.GetRooms();
    for (const auto& room : rooms) {
        std::cout << "Room name: " << room.first << std::endl;
        // Access room details using room.second
    }std::cout << std::endl;*/
 std::vector<std::pair<std::string, std::string>> questions = {
        {"What has keys but can't open locks?", "keyboard"},
        {"What comes once in a minute, twice in a moment, but never in a thousand years?", "m"},
        {"What is full of holes but still holds water?", "Sponge"},
        {"What is always in front of you but can't be seen?", "Future"}

        // Add more questions as needed
    };


  // Game loop
    while (true) {
        std::cout << "Current Location: " << player.GetLocation()->GetDescription() << std::endl;

        // Print items in the room
        if (!player.GetLocation()->GetItems().empty()) {
            std::cout << "Items in the room:" << std::endl;
            for (const Item& item : player.GetLocation()->GetItems()) {
                std::cout << "- " << item.getName() << ": " << item.getDescription() << std::endl;
            }
        }

        // Print available exits
        std::cout << "Available exits: ";
        for (const auto& exit : player.GetLocation()->GetExits()) {
            std::cout << exit.first << " ";
        }
        std::cout << std::endl;

        // Print options
        std::cout << "Options: ";
        std::cout << "1. Look around | ";
        std::cout << "2. Interact with an item | ";
        std::cout << "3. Move to another room | ";
        std::cout << "4. Quit" << std::endl;

        int choice;
        std::cin >> choice;

        if (choice == 1) {
            std::cout << "You look around the room." << std::endl;
        } else if (choice == 2) {
    std::cout << "Enter the name of the item you want to interact with: ";
    std::string itemName;
    std::cin >> itemName;

    // Get the item object from the room
    Item* item = player.GetLocation()->GetItem(itemName);

    if (item != nullptr) {
        // Handle the enigmatic challenge
        std::cout << "You encountered an enigmatic challenge! Solve it to pick up the item." << std::endl;
        bool challengePassed = player.SolveEnigmaticChallenge(questions);

        if (challengePassed) {
            // If the challenge is passed, remove the item from the room
            player.GetLocation()->RemoveItem(itemName);
            std::cout << "You pick up the " << item->getName() << "." << std::endl;
        } else {
            // If the challenge is failed, inform the player
            std::cout << "You failed to solve the challenge. The item remains untouched." << std::endl;
            player.TakeDamage(10); // Decrease player's health
                    std::cout << "Your health is now: " << player.GetHealth() << std::endl;
        }
    } else {
        std::cout << "There is no item with that name in the room." << std::endl;
    }
        }



else if (choice == 3) {
           std::cout << "Enter the direction (e.g., north, south): ";
std::string direction;
std::cin >> direction;
std::cout << "Direction entered: " << direction << std::endl;

Room* nextRoom = player.GetLocation()->GetExit(direction);
if (nextRoom != nullptr) {
    player.SetLocation(nextRoom);
    std::cout << "You move to the next room." << std::endl;
} else {
    std::cout << "You can't go that way." << std::endl;
}

 } else if (choice == 4) {
        std::cout << "Goodbye!" << std::endl;
        break;
    } else {
        std::cout << "Invalid choice. Try again." << std::endl;
    }

    // Check if the player's health has reached zero
    if (player.GetHealth() <= 0) {
        std::cout << "Your health has reached zero. You died!" << std::endl;
        break; // End the game loop
    }
}

    return 0;
}



