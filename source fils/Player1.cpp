#include "../include/Item1.h"
#include "../include/Player1.h"
#include "../include/Character1.h"

Player::Player(const std::string& name, int health, Room* startingRoom): Character(name, health), location(startingRoom ) {}

void Player::SetLocation(Room* room){
    location = room;
}

Room* Player::GetLocation(){
    return location;
}


void Player::PickupItem(const Item& item) {
    // Assuming Character class has a method to access inventory
    getInventory().push_back(item);

}


bool Player::SolveEnigmaticChallenge(const std::vector<std::pair<std::string, std::string>>& questions) {
    srand(time(nullptr));

    // Ensure questions vector is not empty
    if (questions.empty()) {
        std::cout << "No questions available." << std::endl;
        return false;
    }

    // Shuffle the questions
    std::vector<std::pair<std::string, std::string>> shuffledQuestions = questions;
    std::random_shuffle(shuffledQuestions.begin(), shuffledQuestions.end());

    // Select a random question
    int index = rand() % shuffledQuestions.size();
    std::string question = shuffledQuestions[index].first;
    std::string correctAnswer = shuffledQuestions[index].second;

    // Present the challenge to the player
    std::cout << "Riddle: " << question << std::endl;

    // Get the player's input
    std::string answer;
    std::cout << "Your answer: ";
    std::cin >> answer;

    // Check if the answer is correct
    bool isCorrect = (answer == correctAnswer);
    if (isCorrect) {
        std::cout << "Correct!" << std::endl;
    } else {
        std::cout << "Incorrect!" << std::endl;
        TakeDamage(10);
    }

    return isCorrect;
}


