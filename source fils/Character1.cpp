#include "../include/Character1.h"
#include "../include/Room1.h"
#include "../include/Item1.h"
#include "../include/Area.h"


Character::Character(const std::string& name, int health): name(name), health(health){}


 std::vector<Item>& Character::getInventory() {
    return inventory;
}

void Character::TakeDamage(int damage){
      health -= damage;
        if (health < 0) {
            health = 0; // Health cannot be negative
}
}
int Character::GetHealth() const {
    return health;
}
