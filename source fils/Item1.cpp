#include "Item1.h"
#include "Room1.h"
#include "Character1.h"
#include "Player1.h"
#include "Area.h"

Item::Item(const std::string& name, const std::string& desc): name(name), description(desc){}

std::string Item::getName() const {
    return name;
}

std::string Item::getDescription() const {
    return description;
}

void Item::Interact(){
    std::cout << "Interact with item: "<< name << std::endl;
    std::cout << "description: "<< description << std::endl;

}

