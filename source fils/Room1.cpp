#include "Room1.h"
#include "Item1.h"
#include "Area.h"
#include "Character1.h"
#include "Player1.h"

Room::Room(const std::string& desc): description(desc){}

std::string Room::GetDescription() const {
    return description;
}

void Room::AddItem(const Item& item){
    items.push_back(item);

}

const std::vector<Item>& Room::GetItems() const {
    return items;
}

Item* Room::GetItem(const std::string& name) {
    for (Item& item : items) {
        if (item.getName() == name) {
            return &item;
        }
    }
    return nullptr; // Return nullptr if no matching item is found
}

void Room::RemoveItem(const std::string& itemName) {
    // Find the item in the room's inventory
    auto it = std::find_if(items.begin(), items.end(), [&](const Item& item) {
        return item.getName() == itemName;
    });

    // If the item is found, remove it
    if (it != items.end()) {
        items.erase(it);
    }
}



void Room::AddExit(const std::string& direction, Room* room){
    exits[direction] = room;

}
const std::map<std::string, Room*>& Room::GetExits() const {
    return exits;
}
Room* Room::GetExit(const std::string& direction) {
    if (exits.find(direction) != exits.end()) {
        return exits[direction];
    } else {
        // Return nullptr if the direction doesn't exist
        return nullptr;
    }
}


void Room:: DisplayItems(){
std::cout << "Room:" << description << std::endl;
std::cout << "Items in this room:" << std::endl;

        for (const auto& item : items) {
          std::cout << "- " << item.getName() << ": " << item.getDescription() << std::endl;

        }
    }




